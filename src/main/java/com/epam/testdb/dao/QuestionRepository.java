package com.epam.testdb.dao;

import com.epam.testdb.domain.Question;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface QuestionRepository extends CrudRepository<Question, Long> {

    List<Question> findByQuizId(long quizId);

}
