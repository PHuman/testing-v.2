package com.epam.testdb.dao;

import com.epam.testdb.domain.Quiz;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface QuizRepository extends CrudRepository<Quiz, Long> {

	List<Quiz> findByTopicId(long topicId);
}
