package com.epam.testdb.domain;

import java.io.Serializable;

public class Result implements Serializable {
	private static final long serialVersionUID = -6944952251989621076L;
	private long id;
	private long result;

	public Result() {
	}

	public Result(long result) {
		this.result = result;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getResult() {
		return result;
	}

	public void setResult(long result) {
		this.result = result;
	}

}
