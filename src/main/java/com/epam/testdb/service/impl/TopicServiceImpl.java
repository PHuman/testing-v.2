package com.epam.testdb.service.impl;

import com.epam.testdb.dao.TopicRepository;
import com.epam.testdb.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicServiceImpl implements TopicService {
    @Autowired
    private TopicRepository repository;

    @Override
    public TopicRepository getRepository() {
        return repository;
    }
}
