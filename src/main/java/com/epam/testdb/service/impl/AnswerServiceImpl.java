package com.epam.testdb.service.impl;

import com.epam.testdb.dao.AnswerRepository;
import com.epam.testdb.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;

public class AnswerServiceImpl implements AnswerService {
    @Autowired
    private AnswerRepository repository;

    @Override
    public AnswerRepository getRepository() {
        return repository;
    }
}
