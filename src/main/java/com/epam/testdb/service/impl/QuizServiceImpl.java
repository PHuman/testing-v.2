package com.epam.testdb.service.impl;

import com.epam.testdb.dao.QuizRepository;
import com.epam.testdb.service.QuizService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuizServiceImpl implements QuizService {
    @Autowired
    private QuizRepository repository;

    @Override
    public QuizRepository getRepository() {
        return repository;
    }
}
