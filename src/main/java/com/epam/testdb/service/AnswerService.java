package com.epam.testdb.service;

import com.epam.testdb.dao.AnswerRepository;
import com.epam.testdb.domain.Answer;

public interface AnswerService extends CrudService<AnswerRepository, Answer> {
}
