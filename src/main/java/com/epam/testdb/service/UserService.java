package com.epam.testdb.service;

import com.epam.testdb.dao.UserRepository;
import com.epam.testdb.domain.User;

public interface UserService extends CrudService<UserRepository, User> {
}
