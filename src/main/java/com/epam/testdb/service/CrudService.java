package com.epam.testdb.service;

import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;

interface CrudService<T extends CrudRepository<R, Long>, R extends Serializable> {
    T getRepository();

    default R fetch(long id) {
        return getRepository().findOne(id);
    }

    default void save(R r){
        getRepository().save(r);
    }

    default void delete(long id){
        getRepository().delete(id);
    }
}
