package com.epam.testdb.service;

import java.util.List;

import com.epam.testdb.dao.QuizRepository;
import com.epam.testdb.domain.Quiz;

public interface QuizService extends CrudService<QuizRepository,Quiz> {
	
	default List<Quiz> findByTopicId(long topicId){
		return getRepository().findByTopicId(topicId);
	}
}
