package com.epam.testdb.service;

import com.epam.testdb.dao.QuestionRepository;
import com.epam.testdb.domain.Question;
import com.epam.testdb.domain.Result;

import java.util.List;

public interface QuestionService extends CrudService<QuestionRepository, Question> {

    default List<Question> findByQuizId(long quizId) {
        List<Question> questions = getRepository().findByQuizId(quizId);
        questions.forEach(q -> q.getAnswers().forEach(a -> a.setAnswer(false)));
        return questions;
    }

    default Result checkResult(long quizId, List<Question> result) {
        List<Question> expected = getRepository().findByQuizId(quizId);
        long count = result.stream()
                .filter(q -> expected.stream().anyMatch(q::equals))
                .count();
        return new Result(count * 100 / result.size());
    }
}

