package com.epam.testdb.service;

import com.epam.testdb.dao.TopicRepository;
import com.epam.testdb.domain.Topic;

import java.util.List;

public interface TopicService extends CrudService<TopicRepository, Topic> {

    default List<Topic> fetchAll() {
        return (List<Topic>) getRepository().findAll();
    }
}
