package com.epam.testdb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.testdb.domain.User;
import com.epam.testdb.service.UserService;

@CrossOrigin
@RestController
public class UserController {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/")
	public String showIndex(Model model) {
		model.addAttribute("user", new User());
		return "test/topic";
		// return "authorization/login";
	}

	@RequestMapping(value = "login")
	public String login(Model model) {
		return "test/topic";
	}

	@RequestMapping(value = "registration")
	public String registration(Model model, @ModelAttribute("user") User user) {
		if (user.getName() == null) {
			model.addAttribute("user", user);
			return "authorization/registration";
		}
		userService.save(user);
		return "authorization/login";
	}

}
