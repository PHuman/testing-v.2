package com.epam.testdb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.testdb.domain.Quiz;
import com.epam.testdb.service.QuizService;

@CrossOrigin
@RestController
public class QuizController {
	@Autowired
	private QuizService quizService;

	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = "/topic/{topicId}")
	List<Quiz> findByTopicId(@PathVariable long topicId) {
		return quizService.findByTopicId(topicId);
	}	
}
