package com.epam.testdb.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.testdb.domain.Topic;
import com.epam.testdb.service.TopicService;

@CrossOrigin
@RestController
public class TopicController {
	@Autowired
	private TopicService topicService;

	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = "/topic/topics")
	public List<Topic> fetchAll(HttpServletResponse response) {
		return topicService.fetchAll();
	}
}
