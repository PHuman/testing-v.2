package com.epam.testdb.controller;

import com.epam.testdb.domain.Question;
import com.epam.testdb.domain.Result;
import com.epam.testdb.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class QuestionController {
    @Autowired
    private QuestionService questionService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/topic/{topicId}/{quizId}")
    List<Question> findByQuizId(@PathVariable long quizId) {
        return questionService.findByQuizId(quizId);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/topic/{topicId}/{quizId}/result")
    Result checkResult(@PathVariable long quizId, @RequestBody List<Question> result) {
        return questionService.checkResult(quizId, result);
    }
}
