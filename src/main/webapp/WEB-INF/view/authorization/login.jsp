<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<script type="text/javascript" src="/usr/resources/js/script.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body>
	<f:form action="login" method="post" id="login" modelAttribute="user">
<%-- 		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> --%>		
 		<f:input type="text" placeholder="login" name="login" path="login" required="required"/>
		<f:input type="password" placeholder="password" path="password" required="required"/>
		<input type="submit" value="login"/>
		<input type="submit" value="registration" onclick="reg()"/>
	</f:form>
</body>
</html>