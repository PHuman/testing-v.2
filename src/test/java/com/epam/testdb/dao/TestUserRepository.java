package com.epam.testdb.dao;

import com.epam.testdb.domain.User;
import com.epam.testdb.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/spring-context.xml")
public class TestUserRepository {
    private User user;
    @Autowired
    private UserRepository repository;
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private UserService service;

    @Before
    public void init() {
        user = new User();
        user.setName("Pavel");
        user.setLogin("Pavel");
        user.setPassword("81dc9bdb52d04dc20036dbd8313ed055");
    }

    @Test
    public void testFindById() {
        User one = service.fetch(1L);
        System.out.println(one);
        Assert.assertEquals(one.getName().trim(), "Pavel");
    }
}
